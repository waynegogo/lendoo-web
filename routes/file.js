'use strict';
var router = require('express').Router();
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var path = require('path');
var fs = require('fs');
var AV = require('leanengine');

// 添加分类
router.post('/upload', multipartMiddleware, function(req, res) {
    console.log('uploading');
    // 临时文件完整路径
    var file_url = req.files.file.path;
    // 截取文件名
    var filename = path.basename(file_url);
    // 向AV上传文件
    var file = new AV.File(filename, fs.readFileSync(file_url));
    file.save().then(function(file) {
        // 文件保存成功
        console.log(file.url());
        res.send(file);
    }, function(error) {
        // 异常处理
        console.error(error);
    });
});

module.exports = router;